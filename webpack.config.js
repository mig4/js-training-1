const path = require('path');
module.exports = {
	devtool: 'inline-source-map',
	devServer: {
//		contentBase: './build'
		publicPath: '/build/'
	},
	entry: {
		app: ['./index.js']
	},
	output: {
		path: path.resolve(__dirname, 'build'),
		filename: 'bundle.js'
	}
}
