import _ from 'lodash';

/**
 * create a component with a welcome message
 *
 * @return {Object} div element
 */
function helloComponent() {
	let element = document.createElement('div');

	element.innerHTML = _.join(['Hello', 'webpack', '!!!'], ' ');

	return element;
}

document.body.appendChild(helloComponent());
